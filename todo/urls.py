from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from . import views

urlpatterns = [
    path('todos/', views.TodoList.as_view()),
    path('todo/<int:pk>/', views.TodoDetail.as_view()),
    path('auth/login/', obtain_auth_token, name='auth_user_login'),
    path('auth/register/', views.CreateUserAPIView.as_view(), name='auth_user_create'),
    path('auth/logout/', views.LogoutUserAPIView.as_view(), name='auth_user_logout')
]
