from django.db import models

# Create your models here.


class Todo(models.Model):
    PRIORITY_CHOICES = (
        ('high', 'High'),
        ('low', 'Low'),
        ('medium', 'Medium'),
    )

    description = models.CharField(max_length=40)
    priority = models.CharField(max_length=20, choices=PRIORITY_CHOICES)

    def __str__(self):
        return '{} - {}'.format(self.description, self.get_priority_display())
